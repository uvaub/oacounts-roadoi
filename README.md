# README #

This is a simple wrapper script for a tool called [roadoi](https://github.com/njahn82/roadoi.git).
Roadoi is a client app that connects to the [oaDOI](https://oadoi.org/) web service, which provides information on the open access state of publications.

## Why use this wrapper script for roadoi ##

This wrapper script is specifically written for our use case (University of Amsterdam), in which we take another tool called [oa-scraper](https://github.com/utwente/oa-scraper.git) that checks the (oa) availability of publications outside of the University realm (e.g. using VPN or at home). The result of this tool is enriched with the information obtained from oaDOI.

* Howto use this wrapper script

Download the [oa-scraper](https://github.com/utwente/oa-scraper.git) tool and add your institution's publications (DOI) to it. The tool runs embedded within the MS Excel application using macros. Export the result to CSV format.

Note that in our workflow we have made our own classification for grouping publications. This information is put into the MS Excel sheet as well (the data come from our CRIS). This variable is called Open.Access.classification in the script, it is not important for roadoi.

This wrapper script assumes the input file containing the DOIs to be fed to oaDOI is called 'input-dois.csv'.

### Who do I talk to? ###
[Digital Production Center](http://uba.uva.nl/en/services/other/dpc) of the Library of the University of Amsterdam.